resource "aws_dynamodb_table" "basic-table-test" {
  name                           = "table-test"
  billing_mode                   = "PROVISIONED"
  read_capacity                  =20  
  write_capacity                 =20
  hash_key                       = "id"
  
  attribute {
      name = "id"
      type = "S"
    }
	ttl{
	attribute_name="TimeToExist"
	enabled       =false
	}  

  tags = {
    Name        ="dynamodb-table-test"  
    Environment = "Dev"
  }
}
